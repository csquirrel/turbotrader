//
//  BackgroundInformationPage.m
//  TurboTrader
//
//  Created by Marcin Maciukiewicz on 01/05/2015.
//  Copyright (c) 2015 Marcin Maciukiewicz. All rights reserved.
//

#import "BackgroundInformationPage.h"

@interface BackgroundInformationPage ()

@end

@implementation BackgroundInformationPage

- (BOOL)validatePage {
    
    BOOL result = YES;
    result &= [_streetAddress validateSelf];
    result &= [_city validateSelf];
    result &= [_state validateSelf];
    result &= [_zipCode validateSelf];
    result &= [_dateMovedIn validateSelf];
    
    return result;
}

- (void)showNextPage {
    [self performSegueWithIdentifier:@"exitPage" sender:self];
}


- (void)viewDidLoad {
    [super viewDidLoad];

    [self showGotoNext:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    BackgroundInformation *backgroundInformation = [[[BackgroundInformation alloc] init] autorelease];
    self.registrationDetails.backgroundInformation = backgroundInformation;
    backgroundInformation.streetAddress = _streetAddress.text;
    backgroundInformation.city = _city.text;
    backgroundInformation.state = _state.text;
    backgroundInformation.zipCode = _zipCode.text;
    backgroundInformation.dateMovedIn = _dateMovedIn.selectedDate;

    [super prepareForSegue:segue sender:sender];
    
}

#pragma mark ValidationResultDelegate
- (void)textField:(UITextField *)textField isValid:(BOOL)isValid {
    
    UIColor *color = (isValid?[UIColor whiteColor]:[UIColor redColor]);
    textField.backgroundColor = color;
}

- (void) dealloc {
    
    [_streetAddress release];
    [_city release];
    [_state release];
    [_zipCode release];
    [_dateMovedIn release];
    
    [super dealloc];
}

@end
