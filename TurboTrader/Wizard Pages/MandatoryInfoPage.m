//
//  MandatoryInfoPage.m
//  TurboTrader
//
//  Created by Marcin Maciukiewicz on 01/05/2015.
//  Copyright (c) 2015 Marcin Maciukiewicz. All rights reserved.
//

#import "MandatoryInfoPage.h"

@interface MandatoryInfoPage ()

@property (nonatomic, retain) NSDate *selectedDate;

@end

@implementation MandatoryInfoPage

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    _sex.values = @[
                    NSLocalizedStringFromTable(@"SexMale", @"MandatoryInfoPage", @""),
                    NSLocalizedStringFromTable(@"SexFemale", @"MandatoryInfoPage", @"")
                    ];
    
    [self showGotoNext:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    // reset the registration detail every time user navigates to this screen
    self.registrationDetails = [[[RegistrationDetails alloc] init] autorelease];
}

- (BOOL)validatePage {
    
    BOOL result = YES;
    result &= [_firstName validateSelf];
    result &= [_lastName validateSelf];
    result &= [_sex validateSelf];
    result &= [_dateOfBirth validateSelf];
    
    return result;
}

- (void)showNextPage {
    
    NSString *countryId;
    countryId = [[NSLocale preferredLanguages] objectAtIndex:0];

    if([countryId isEqualToString:@"en"]) {
        // US customers
        [self performSegueWithIdentifier:@"backgroundInformation" sender:self];
    } else if([countryId isEqualToString:@"de"]) {
        // German customers
        [self performSegueWithIdentifier:@"financialDetails" sender:self];
    } else {
        // British ( and other nationalities) customers
        [self performSegueWithIdentifier:@"exitPage" sender:self];
    }
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    MandatoryDetails *mandatoryDetails = [[[MandatoryDetails alloc] init] autorelease];
    self.registrationDetails.mandatoryDetails = mandatoryDetails;
    mandatoryDetails.firstName = self.firstName.text;
    mandatoryDetails.lastName = self.lastName.text;
    if(self.sex.text.length > 0) {
        NSString *maleString = NSLocalizedStringFromTable(@"SexMale", @"MandatoryInfoPage", @"");
        BOOL isMale = [self.sex.text isEqualToString:maleString];
        mandatoryDetails.isMale = [NSNumber numberWithBool:isMale];
    }
    mandatoryDetails.dateOfBirth = _dateOfBirth.selectedDate;

    [super prepareForSegue:segue sender:sender];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark ValidationResultDelegate
- (void)textField:(UITextField *)textField isValid:(BOOL)isValid {
  
    UIColor *color = (isValid?[UIColor whiteColor]:[UIColor colorWithRed:1.0 green:0.5 blue:0.5 alpha:1.0]);
    textField.backgroundColor = color;
}

- (void) dealloc {
    
    [_firstName release];
    [_lastName release];
    [_sex release];
    [_dateOfBirth release];
    
    [super dealloc];
}

@end
