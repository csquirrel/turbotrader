//
//  BackgroundInformationPage.h
//  TurboTrader
//
//  Created by Marcin Maciukiewicz on 01/05/2015.
//  Copyright (c) 2015 Marcin Maciukiewicz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WizardPage.h"
#import "NonEmptyTextField.h"
#import "DateTextField.h"

@interface BackgroundInformationPage : WizardPage <ValidationResultDelegate>

@property (nonatomic, retain) IBOutlet NonEmptyTextField *streetAddress;
@property (nonatomic, retain) IBOutlet NonEmptyTextField *city;
@property (nonatomic, retain) IBOutlet NonEmptyTextField *state;
@property (nonatomic, retain) IBOutlet NonEmptyTextField *zipCode;
@property (nonatomic, retain) IBOutlet DateTextField *dateMovedIn;

@end
