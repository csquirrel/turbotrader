//
//  FinancialDetailsPage.m
//  TurboTrader
//
//  Created by Marcin Maciukiewicz on 01/05/2015.
//  Copyright (c) 2015 Marcin Maciukiewicz. All rights reserved.
//

#import "FinancialDetailsPage.h"

@interface FinancialDetailsPage ()

@end

@implementation FinancialDetailsPage

- (BOOL)validatePage {
    
    BOOL result = YES;
    result &= [_annualIncome validateSelf];
    result &= [_annualExpenses validateSelf];
    
    return result;
}

- (void)showNextPage {
    [self performSegueWithIdentifier:@"exitPage" sender:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self showGotoNext:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    FinancialDetails *financialDetails = [[[FinancialDetails alloc] init] autorelease];
    self.registrationDetails.financialDetails = financialDetails;
    financialDetails.annualIncome = self.annualIncome.text;
    financialDetails.annualExpenses = self.annualExpenses.text;
    
    [super prepareForSegue:segue sender:sender];
    
}

#pragma mark ValidationResultDelegate
- (void)textField:(UITextField *)textField isValid:(BOOL)isValid {
    
    UIColor *color = (isValid?[UIColor whiteColor]:[UIColor redColor]);
    textField.backgroundColor = color;
}

- (void) dealloc {
    
    [_annualIncome release];
    [_annualExpenses release];
    
    [super dealloc];
}

@end
