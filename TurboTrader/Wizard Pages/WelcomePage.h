//
//  WelcomePage.h
//  TurboTrader
//
//  Created by Marcin Maciukiewicz on 01/05/2015.
//  Copyright (c) 2015 Marcin Maciukiewicz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WizardPage.h"

@interface WelcomePage : UIViewController

@end
