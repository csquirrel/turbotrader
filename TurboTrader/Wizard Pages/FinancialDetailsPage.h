//
//  FinancialDetailsPage.h
//  TurboTrader
//
//  Created by Marcin Maciukiewicz on 01/05/2015.
//  Copyright (c) 2015 Marcin Maciukiewicz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WizardPage.h"
#import "NonEmptyTextField.h"
#import "DateTextField.h"

@interface FinancialDetailsPage : WizardPage <ValidationResultDelegate>

@property(nonatomic, retain) IBOutlet NonEmptyTextField *annualIncome;
@property(nonatomic, retain) IBOutlet NonEmptyTextField *annualExpenses;

@end
