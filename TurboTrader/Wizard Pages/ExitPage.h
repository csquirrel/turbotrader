//
//  ExitPage.h
//  TurboTrader
//
//  Created by Marcin Maciukiewicz on 01/05/2015.
//  Copyright (c) 2015 Marcin Maciukiewicz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WizardPage.h"

@interface ExitPage : WizardPage

@property (nonatomic, retain) IBOutlet UIView *registrationInProgress;
@property (nonatomic, retain) IBOutlet UIView *registrationSuccessful;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *progressIndicator;

@end
