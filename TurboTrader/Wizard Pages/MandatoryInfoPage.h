//
//  MandatoryInfoPage.h
//  TurboTrader
//
//  Created by Marcin Maciukiewicz on 01/05/2015.
//  Copyright (c) 2015 Marcin Maciukiewicz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WizardPage.h"
#import "ValidatedTextField.h"
#import "NonEmptyTextField.h"
#import "SetTextField.h"
#import "DateTextField.h"

@interface MandatoryInfoPage : WizardPage <ValidationResultDelegate>

@property(nonatomic, retain) IBOutlet NonEmptyTextField *firstName;
@property(nonatomic, retain) IBOutlet NonEmptyTextField *lastName;
@property(nonatomic, retain) IBOutlet SetTextField *sex;
@property(nonatomic, retain) IBOutlet DateTextField *dateOfBirth;

@end
