//
//  WizardPage.m
//  TurboTrader
//
//  Created by Marcin Maciukiewicz on 04/05/2015.
//  Copyright (c) 2015 Marcin Maciukiewicz. All rights reserved.
//

#import "WizardPage.h"

@interface WizardPage ()

@property (nonatomic, retain) UITextField *activeTextField;

@end

@implementation WizardPage

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
}

- (void) viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self registerForKeyboardNotifications];
}

- (void) viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [self deregisterForKeyboardNotifications];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)deregisterForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    

    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
    _scrollView.contentInset = contentInsets;
    _scrollView.scrollIndicatorInsets = contentInsets;

}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _scrollView.contentInset = contentInsets;
    _scrollView.scrollIndicatorInsets = contentInsets;
}

- (void)showNextPage {
    // to be implemented in subclasses
}

- (BOOL)validatePage {
 
   // to be implemented in subclasses
    return YES;
}

- (void)canShowNextPage:(UIButton *)sender {

    BOOL isPageValid = [self validatePage];
    if(isPageValid) {
        [self showNextPage];
    } else {
        UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:NSLocalizedStringFromTable(@"ErrorAlertTitle", @"WizardPage", @"")
                                                        message:NSLocalizedStringFromTable(@"ErrorAlertDescription", @"WizardPage", @"")
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedStringFromTable(@"ErrorAlertButton", @"WizardPage", @"")
                                              otherButtonTitles:nil] autorelease];
        [alert show];
    }
}

- (void)showGotoNext:(BOOL)shouldShow {
    
    if(shouldShow) {
        if (!_gotoNext) {
            self.gotoNext = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedStringFromTable(@"NextButton", @"WizardPage", @"")
                                                                         style:UIBarButtonItemStylePlain
                                                                        target:self
                                                                        action:@selector(canShowNextPage:)];
        }
    } else {
        self.gotoNext = nil;
    }
    self.navigationItem.rightBarButtonItem = _gotoNext;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.destinationViewController isKindOfClass:[WizardPage class]]) {
        ((WizardPage*)segue.destinationViewController).registrationDetails = self.registrationDetails;
    }
    
}

- (void) dealloc {
    
    [_registrationDetails release];
    [_gotoNext release];
    [_scrollView release];
    
    [super dealloc];
    
}

@end
