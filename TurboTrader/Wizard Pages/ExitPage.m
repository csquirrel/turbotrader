//
//  ExitPage.m
//  TurboTrader
//
//  Created by Marcin Maciukiewicz on 01/05/2015.
//  Copyright (c) 2015 Marcin Maciukiewicz. All rights reserved.
//

#import "ExitPage.h"
#import "AccountService.h"

@interface ExitPage ()

    @property(nonatomic, retain) id<CancellableRequest> pendingRequest;

@end

@implementation ExitPage

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [_progressIndicator startAnimating];
    
    AccountService *srv = [[AccountService alloc] init];

    __block ExitPage *exitPage = self;
    self.pendingRequest = [srv registerUser:self.registrationDetails
                                   callback:^(NSError *error) {
    
        dispatch_async(dispatch_get_main_queue(), ^{
            [_progressIndicator stopAnimating];
            
            if(error) {
                [exitPage failedToRegister:error];
            } else {
                [exitPage registeredSuccessful];
            }
        });
       exitPage.pendingRequest = nil;
    }];
}

- (void) viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [self.pendingRequest cancel];
    
}

- (void) failedToRegister:(NSError*)error {
    NSLog(@"DEBUG: Failed to register");
}

- (void) registeredSuccessful {
    
    [UIView animateWithDuration:1 animations:^{
        _registrationInProgress.alpha = 0.0;
        _registrationSuccessful.alpha = 1.0;
    } completion:^(BOOL finished) {

    }];
    
}

- (void) dealloc {
    
    [super dealloc];
}

@end
