//
//  WizardPage.h
//  TurboTrader
//
//  Created by Marcin Maciukiewicz on 04/05/2015.
//  Copyright (c) 2015 Marcin Maciukiewicz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RegistrationDetails.h"

/**
 Common parent for all the wizard pages. 
 Responsibilities:
  - holds an instance of registration details
  - presents "next" button when requested
  - responds to keyboard notifications and scrolls content
 
 Note: Subclasses are expected to overwrite showNextPage and validatePage methods. 
 */
@interface WizardPage : UIViewController <UITextFieldDelegate>

@property(nonatomic, retain) RegistrationDetails *registrationDetails;
@property(nonatomic, retain) UIBarButtonItem *gotoNext;
@property(nonatomic, retain) IBOutlet UIScrollView *scrollView;

- (void)showNextPage;

- (void)showGotoNext:(BOOL)shouldShow;

- (BOOL)validatePage;

@end
