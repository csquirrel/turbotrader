//
//  BackgroundInformation.h
//  TurboTrader
//
//  Created by Marcin Maciukiewicz on 04/05/2015.
//  Copyright (c) 2015 Marcin Maciukiewicz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BackgroundInformation : NSObject

@property (nonatomic, copy) NSString *streetAddress;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *state;
@property (nonatomic, copy) NSString *zipCode;
@property (nonatomic, retain) NSDate *dateMovedIn;

/**
 Turns the object into a json dictionary
 */
- (NSDictionary*)jsonDictionary;

@end
