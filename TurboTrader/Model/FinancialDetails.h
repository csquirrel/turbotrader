//
//  FinancialDetails.h
//  TurboTrader
//
//  Created by Marcin Maciukiewicz on 04/05/2015.
//  Copyright (c) 2015 Marcin Maciukiewicz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FinancialDetails : NSObject

/** Numeric value is handled as a string to avoid conversion issues */
@property(nonatomic, copy) NSString *annualIncome;
/** Numeric value is handled as a string to avoid conversion issues */
@property(nonatomic, copy) NSString *annualExpenses;

- (NSDictionary*)jsonDictionary;

@end
