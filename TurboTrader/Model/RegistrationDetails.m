//
//  RegistrationDetails.m
//  TurboTrader
//
//  Created by Marcin Maciukiewicz on 04/05/2015.
//  Copyright (c) 2015 Marcin Maciukiewicz. All rights reserved.
//

#import "RegistrationDetails.h"

@implementation RegistrationDetails

- (NSDictionary*)jsonDictionary {
    
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    
    if(_mandatoryDetails) {
        NSDictionary *jsonDict = [_mandatoryDetails jsonDictionary];
        if(jsonDict.allKeys.count > 0) {
            result[@"mandatoryDetails"] = jsonDict;
        }
    }
    
    if(_backgroundInformation) {
        NSDictionary *jsonDict = [_backgroundInformation jsonDictionary];
        if(jsonDict.allKeys.count > 0) {
            result[@"backgroundInformation"] = jsonDict;
        }
    }
    
    if(_financialDetails) {
        NSDictionary *jsonDict = [_financialDetails jsonDictionary];
        if(jsonDict.allKeys.count > 0) {
            result[@"financialDetails"] = jsonDict;
        }
    }
    
    return result;
    
}


- (void)dealloc {
    
    [_mandatoryDetails release];
    [_backgroundInformation release];
    [_financialDetails release];
    
    [super dealloc];
    
}

@end
