//
//  MandatoryDetails.h
//  TurboTrader
//
//  Created by Marcin Maciukiewicz on 04/05/2015.
//  Copyright (c) 2015 Marcin Maciukiewicz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MandatoryDetails : NSObject

@property(nonatomic, copy)      NSString *firstName;
@property(nonatomic, copy)      NSString *lastName;
@property(nonatomic, retain)    NSNumber *isMale;
@property(nonatomic, retain)    NSDate *dateOfBirth;

/**
 Turns the object into a json dictionary
 */
- (NSDictionary*)jsonDictionary;

@end
