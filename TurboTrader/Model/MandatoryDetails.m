//
//  MandatoryDetails.m
//  TurboTrader
//
//  Created by Marcin Maciukiewicz on 04/05/2015.
//  Copyright (c) 2015 Marcin Maciukiewicz. All rights reserved.
//

#import "MandatoryDetails.h"

@implementation MandatoryDetails

- (NSDictionary*)jsonDictionary {
    
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    
    if(_firstName.length > 0) {
        result[@"firstName"] = _firstName;
    }
    
    if(_lastName.length > 0) {
        result[@"lastName"] = _lastName;
    }
    
    if(_isMale) {
        result[@"isMale"] = _isMale;
    }
    
    if(_dateOfBirth) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        result[@"dateOfBirth"] = [formatter stringFromDate:_dateOfBirth];
        [formatter release];
    }
    
    return result;
}

- (void)dealloc {
    
    [_firstName release];
    [_lastName release];
    [_isMale release];
    [_dateOfBirth release];
    
    [super dealloc];
}

@end
