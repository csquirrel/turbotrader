//
//  BackgroundInformation.m
//  TurboTrader
//
//  Created by Marcin Maciukiewicz on 04/05/2015.
//  Copyright (c) 2015 Marcin Maciukiewicz. All rights reserved.
//

#import "BackgroundInformation.h"

@implementation BackgroundInformation

- (NSDictionary*)jsonDictionary {
    
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    
    if(_streetAddress.length > 0) {
    result[@"streetAddress"] = _streetAddress;
    }
    
    if(_city.length > 0) {
        result[@"city"] = _city;
    }
    
    if(_state.length > 0) {
        result[@"state"] = _state;
    }
    
    if(_zipCode.length > 0) {
        result[@"zipCode"] = _zipCode;
    }
    
    if(_dateMovedIn) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        result[@"dateMovedIn"] = [formatter stringFromDate:_dateMovedIn];
        [formatter release];
    }
    
    return result;
}

- (void)dealloc {
    
    [_streetAddress release];
    [_city release];
    [_state release];
    [_zipCode release];
    [_dateMovedIn release];
    
    [super dealloc];
}

@end
