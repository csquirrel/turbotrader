//
//  FinancialDetails.m
//  TurboTrader
//
//  Created by Marcin Maciukiewicz on 04/05/2015.
//  Copyright (c) 2015 Marcin Maciukiewicz. All rights reserved.
//

#import "FinancialDetails.h"

@implementation FinancialDetails

- (NSDictionary*)jsonDictionary {
    
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    
    if(_annualIncome.length > 0) {
        result[@"annualIncome"] = _annualIncome;
    }
    
    if(_annualExpenses.length > 0) {
        result[@"annualExpenses"] = _annualExpenses;
    }
    
    return result;
}

@end
