//
//  RegistrationDetails.h
//  TurboTrader
//
//  Created by Marcin Maciukiewicz on 04/05/2015.
//  Copyright (c) 2015 Marcin Maciukiewicz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MandatoryDetails.h"
#import "BackgroundInformation.h"
#import "FinancialDetails.h"

@interface RegistrationDetails : NSObject

/** Mandatory details required from all the customers */
@property(nonatomic, retain) MandatoryDetails *mandatoryDetails;
/** Background information requetsed from US customers */
@property(nonatomic, retain) BackgroundInformation *backgroundInformation;
/** Financial details requetsed from DE customers */
@property(nonatomic, retain) FinancialDetails *financialDetails;

/**
 Turns the object into a json dictionary
 */
- (NSDictionary*)jsonDictionary;

@end
