//
//  UserRegistrationService.h
//  TurboTrader
//
//  Created by Marcin Maciukiewicz on 04/05/2015.
//  Copyright (c) 2015 Marcin Maciukiewicz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RegistrationDetails.h"

/**
 
 Provides way to cancel pending request if requires ( i.e. user navigated away from a page)
 You can observe use when you enter the "Finish" page and go back before mock request finishes.
 
 */
@protocol CancellableRequest

/** Cancel a pending request */
- (void) cancel;

@end

/**
 Response from the server. 
 @param error nil if successful, otherwise a value
 */
typedef void (^AccountServiceResponse)(NSError *error);

/**
 
 Service to handle registration details. 
 In real life it would encapsulate network communication to the server.
 
 */
@interface AccountService : NSObject

/**
 Register user details in a remote server
 
 @param details mandatory. User provided registration details
 @param callback mandatory. Callback to used to provide registration result
 */
- (id<CancellableRequest>) registerUser:(RegistrationDetails*)details
                               callback:(AccountServiceResponse)callback;

@end
