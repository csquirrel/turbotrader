//
//  UserRegistrationService.m
//  TurboTrader
//
//  Created by Marcin Maciukiewicz on 04/05/2015.
//  Copyright (c) 2015 Marcin Maciukiewicz. All rights reserved.
//

#import "AccountService.h"

#define kMockCancellableRequestDelay 2 // seconds

/**
 
 Since this project is restricted and not using network communication
 the following mock object is used instead of real implementation.
 
 Sole purpose of the mock is to delay the response and call back with result
 
 */
@interface MockCancellableRequest: NSObject <CancellableRequest>

@property (nonatomic, copy) AccountServiceResponse callback;
@property (nonatomic, assign, getter=isCancelled) BOOL cancelled;

@end

@implementation MockCancellableRequest

-(id)initWithCallback:(AccountServiceResponse)callback {
    
    self = [super init];
    if(self) {
        self.callback = callback;
        
        __block MockCancellableRequest *request = self;
        dispatch_time_t t = dispatch_time(DISPATCH_TIME_NOW, kMockCancellableRequestDelay * NSEC_PER_SEC);
        dispatch_after(t, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            if(!request.isCancelled) {
                // call back only if request was not cancelled
                NSLog(@"DEBUG: Mock AccountService.registerUser returned successful result");
                request.callback(nil);
            }
        });
    }
    return self;
}

- (void) cancel {
    
    NSLog(@"DEBUG: Mock AccountService.registerUser is cancelled");
    
    _cancelled = YES;
}

@end

@implementation AccountService

- (id<CancellableRequest>) registerUser:(RegistrationDetails*)details
                               callback:(AccountServiceResponse)callback {
    
    if(!details) {
        return nil;
    }
    
    if(!callback) {
        return nil;
    }
    
    NSDictionary *jsonValue = [details jsonDictionary];
    NSError *jsonError;
    NSData *serialisedData = [NSJSONSerialization dataWithJSONObject:jsonValue
                                                             options:NSJSONWritingPrettyPrinted
                                                               error:&jsonError];
    if(!serialisedData) {
        callback(jsonError);
        return nil;
    }
    
    NSString *debugString = [[NSString alloc] initWithData:serialisedData encoding:NSUTF8StringEncoding];
    NSLog(@"DEBUG: Mock AccountService.registerUser will be called with registration details:\n%@", debugString);
    [debugString release];
    
    return [[[MockCancellableRequest alloc] initWithCallback:callback] autorelease];
}

@end
