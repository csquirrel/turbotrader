//
//  NonEmptyStringValidator.m
//  TurboTrader
//
//  Created by Marcin Maciukiewicz on 04/05/2015.
//  Copyright (c) 2015 Marcin Maciukiewicz. All rights reserved.
//

#import "NonEmptyStringValidator.h"

@implementation NonEmptyStringValidator

-(BOOL)validateValue:(NSString *)value {
    
    NSString *trimmedValue = [value stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    BOOL result = [trimmedValue length] > 0;
    
    return result;
}

@end
