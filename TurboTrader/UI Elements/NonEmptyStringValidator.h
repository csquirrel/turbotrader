//
//  NonEmptyStringValidator.h
//  TurboTrader
//
//  Created by Marcin Maciukiewicz on 04/05/2015.
//  Copyright (c) 2015 Marcin Maciukiewicz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ValueValidator.h"

/**
 Requires value to be non empty string
 */
@interface NonEmptyStringValidator : NSObject <ValueValidator>

@end
