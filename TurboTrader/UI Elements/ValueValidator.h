//
//  TextFieldValidator.h
//  TurboTrader
//
//  Created by Marcin Maciukiewicz on 04/05/2015.
//  Copyright (c) 2015 Marcin Maciukiewicz. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 Defines a value validator. 
 Value validators are used by ValidatedTextField subclasses.
 */
@protocol ValueValidator <NSObject>

/**
 Validated a value
 
 @param value YES if value is valid, NO otherwise. 
 */
-(BOOL)validateValue:(NSString *)value;

@end
