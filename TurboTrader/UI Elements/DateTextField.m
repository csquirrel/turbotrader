//
//  DateTextField.m
//  TurboTrader
//
//  Created by Marcin Maciukiewicz on 05/05/2015.
//  Copyright (c) 2015 Marcin Maciukiewicz. All rights reserved.
//

#import "DateTextField.h"
#import "ValueValidator.h"
#import "NonEmptyStringValidator.h"

@interface DateTextField ()

@property (nonatomic, retain) UIDatePicker *datePicker;

@end

@implementation DateTextField

- (void) awakeFromNib {
    
    self.datePicker = [[[UIDatePicker alloc] init] autorelease];
    _datePicker.datePickerMode = UIDatePickerModeDate;
    [_datePicker addTarget:self
                   action:@selector(selectedDateChanged:)
         forControlEvents:(UIControlEventValueChanged)];
    self.inputView = _datePicker;
    
    [super awakeFromNib];
    
}

- (id<ValueValidator>)newValidator {
    return [[NonEmptyStringValidator alloc] init];
}

- (void)selectedDateChanged:(UIDatePicker *)datePicker {
    
    self.selectedDate = [datePicker date];
    self.text = [self formatDate:_selectedDate];
}

- (NSString*)formatDate:(NSDate *)date {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterNoStyle];
    [formatter setLocale:[NSLocale currentLocale]];
    NSString *result = [formatter stringFromDate:_selectedDate];
    [formatter release];
    
    return result;
}

#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if(!_selectedDate) {
        self.selectedDate = [NSDate date];
    }
    
    _datePicker.date = _selectedDate;
    self.text = [self formatDate:_selectedDate];
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    return NO;
}

@end
