//
//  ValidatedTextField.m
//  TurboTrader
//
//  Created by Marcin Maciukiewicz on 04/05/2015.
//  Copyright (c) 2015 Marcin Maciukiewicz. All rights reserved.
//

#import "ValidatedTextField.h"
#import "ValueValidator.h"

@interface ValidatedTextField ()

@property (nonatomic, retain) id<ValueValidator> validator;

@end

@implementation ValidatedTextField

- (void) awakeFromNib {
    
    self.delegate = self;
    
    self.validator = [[self newValidator] autorelease];
    
}

- (id<ValueValidator>) newValidator {
    return nil;
}

- (BOOL)validateSelf {
    
    NSString *value = self.text;
    BOOL result = [self.validator validateValue:value];
    
    if(_validationResultDelegate) {
        [_validationResultDelegate textField:self isValid:result];
    }

    return result;
}

#pragma mark UITextFieldDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    [self validateSelf];
}

- (void)dealloc {
    
    [_validator release];
    [super dealloc];
}


@end
