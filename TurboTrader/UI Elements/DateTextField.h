//
//  DateTextField.h
//  TurboTrader
//
//  Created by Marcin Maciukiewicz on 05/05/2015.
//  Copyright (c) 2015 Marcin Maciukiewicz. All rights reserved.
//

#import "ValidatedTextField.h"

/**
 Text field associated with date picker.
 */
@interface DateTextField : ValidatedTextField

/**
 Date selected by the user in the picker.
 */
@property (nonatomic, retain) NSDate *selectedDate;

@end
