//
//  NonEmptyTextField.h
//  TurboTrader
//
//  Created by Marcin Maciukiewicz on 04/05/2015.
//  Copyright (c) 2015 Marcin Maciukiewicz. All rights reserved.
//

#import "ValidatedTextField.h"

/**
 A text field implementation which requires value to be non empty string
 */
@interface NonEmptyTextField : ValidatedTextField

@end
