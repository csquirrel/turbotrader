//
//  ValidatedTextField.h
//  TurboTrader
//
//  Created by Marcin Maciukiewicz on 04/05/2015.
//  Copyright (c) 2015 Marcin Maciukiewicz. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 
 Allows to receive validation results. 
 Used by view controllers to change text field's apperance if invalid content.
 
 */
@protocol ValidationResultDelegate <NSObject>

- (void)textField:(UITextField *)textField isValid:(BOOL)isValid;

@end

/**
 
 Base for UITextFields with validation added
 
 */
@interface ValidatedTextField : UITextField <UITextFieldDelegate>

/** 
 View controller can plug in here to receive validation results
 */
@property (nonatomic, assign) IBOutlet id<ValidationResultDelegate> validationResultDelegate;

/**
 Allows forced validation. Used during wizard page validation when all the text fields are checked. 
 */
- (BOOL)validateSelf;

@end
