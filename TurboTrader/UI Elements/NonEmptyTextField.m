//
//  NonEmptyTextField.m
//  TurboTrader
//
//  Created by Marcin Maciukiewicz on 04/05/2015.
//  Copyright (c) 2015 Marcin Maciukiewicz. All rights reserved.
//

#import "NonEmptyTextField.h"
#import "NonEmptyStringValidator.h"

@implementation NonEmptyTextField

- (id<ValueValidator>)newValidator {
    
    return [[NonEmptyStringValidator alloc] init];
    
}

@end
