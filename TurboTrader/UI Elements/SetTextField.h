//
//  SetTextField.h
//  TurboTrader
//
//  Created by Marcin Maciukiewicz on 04/05/2015.
//  Copyright (c) 2015 Marcin Maciukiewicz. All rights reserved.
//

#import "ValidatedTextField.h"

/**
 Text field associated with picker. Allows for data entry using picker with preset values.
 */
@interface SetTextField : ValidatedTextField <UIPickerViewDataSource, UIPickerViewDelegate>

/**
 Values to be presented in the picker.
 */
@property (nonatomic, retain) NSArray *values;

@end
