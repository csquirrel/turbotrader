//
//  NonEmptyStringValidatorTests.m
//  TurboTrader
//
//  Created by Marcin Maciukiewicz on 04/05/2015.
//  Copyright (c) 2015 Marcin Maciukiewicz. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NonEmptyStringValidator.h"

@interface NonEmptyStringValidatorTests : XCTestCase

@end

@implementation NonEmptyStringValidatorTests

- (void)testRejectsNil {
    
    // prepare
    NonEmptyStringValidator *validator = [[[NonEmptyStringValidator alloc] init] autorelease];
    NSString *testString = nil;
    
    // execute
    BOOL result = [validator validateValue:testString];
    
    // verify
    XCTAssertFalse(result, @"Should reject nil string");
}

- (void)testRejectsEmpty {
    
    // prepare
    NonEmptyStringValidator *validator = [[[NonEmptyStringValidator alloc] init] autorelease];
    NSString *testString = @"";
    
    // execute
    BOOL result = [validator validateValue:testString];
    
    // verify
    XCTAssertFalse(result, @"Should reject empty string");
}

- (void)testRejectsWhiteSpace {
    
    // prepare
    NonEmptyStringValidator *validator = [[[NonEmptyStringValidator alloc] init] autorelease];
    NSString *testString = @"        ";
    
    // execute
    BOOL result = [validator validateValue:testString];
    
    // verify
    XCTAssertFalse(result, @"Should reject white space string");
}

- (void)testRejectsAcceptsNonEmpty {
    
    // prepare
    NonEmptyStringValidator *validator = [[[NonEmptyStringValidator alloc] init] autorelease];
    NSString *testString = @"non empty value";
    
    // execute
    BOOL result = [validator validateValue:testString];
    
    // verify
    XCTAssertTrue(result, @"Should accept non empty string");
}


@end
