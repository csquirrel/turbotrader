### TurboTrader Project's Notes ###

* App uses mock class to mimic REST service. Details of the call are displayed in the console.
* The app mocks asynchronous service with 2 second delay. Screen is updated accordingly. 
* App uses user's preferred language to distinguish between US, DE and GB.
* Translation for German language is provided with help of Google Translate. 
* Minimum of unit testing is presented ( due to time limitation )